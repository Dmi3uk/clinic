package dmitruk.clinic.service;

import dmitruk.clinic.model.Doctor;

import java.util.List;

public interface ClinicService {

    List<Doctor> allDoctors(int page);
    int doctorsCount();
}
