package dmitruk.clinic.service;

import dmitruk.clinic.dao.ClinicDAO;
import dmitruk.clinic.model.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClinicServiceImpl implements ClinicService{
    private ClinicDAO clinicDAO;
    @Autowired
    public void setClinicDAO(ClinicDAO clinicDAO) {
        this.clinicDAO = clinicDAO;
    }

    @Override
    @Transactional
    public List<Doctor> allDoctors(int page) {
        return clinicDAO.allDoctors(page);
    }

    @Override
    @Transactional
    public int doctorsCount() {
        return clinicDAO.doctorsCount();
    }
}
