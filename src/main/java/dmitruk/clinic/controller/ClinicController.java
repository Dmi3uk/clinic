package dmitruk.clinic.controller;

import dmitruk.clinic.model.Doctor;
import dmitruk.clinic.service.ClinicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ClinicController {
    private int page;

    private ClinicService clinicService;

    @Autowired
    public void setFilmService(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView allDoctors(@RequestParam(defaultValue = "1") int page) {
        List<Doctor> doctors = clinicService.allDoctors(page);
        int doctorsCount = clinicService.doctorsCount();
        int pagesCount = (doctorsCount + 9)/10;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("clinic");
        modelAndView.addObject("page", page);
        modelAndView.addObject("doctorsList", doctors);
        modelAndView.addObject("doctorsCount", doctorsCount);
        modelAndView.addObject("pagesCount", pagesCount);
        this.page = page;
        return modelAndView;
    }
}
