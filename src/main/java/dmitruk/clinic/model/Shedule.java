package dmitruk.clinic.model;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "shedule")
public class Shedule {
    @Id
    @Column(name = "id_shedule")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_shedule;

    @Column(name = "id_doctor")
    private int id_doctor;

    @Column(name = "working_hours_start")
    private LocalTime working_hours_start;

    @Column(name = "working_hours_end")
    private LocalTime working_hours_end;

    @Column(name = "shedule_date")
    private LocalDate shedule_date;

    public int getId_shedule() {
        return id_shedule;
    }

    public void setId_shedule(int id_shedule) {
        this.id_shedule = id_shedule;
    }

    public int getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(int id_doctor) {
        this.id_doctor = id_doctor;
    }

    public LocalTime getWorking_hours_start() {
        return working_hours_start;
    }

    public void setWorking_hours_start(LocalTime working_hours_start) {
        this.working_hours_start = working_hours_start;
    }

    public LocalTime getWorking_hours_end() {
        return working_hours_end;
    }

    public void setWorking_hours_end(LocalTime working_hours_end) {
        this.working_hours_end = working_hours_end;
    }

    public LocalDate getShedule_date() {
        return shedule_date;
    }

    public void setShedule_date(LocalDate shedule_date) {
        this.shedule_date = shedule_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shedule)) return false;
        Shedule shedule = (Shedule) o;
        return getId_shedule() == shedule.getId_shedule() &&
                getId_doctor() == shedule.getId_doctor() &&
                Objects.equals(getWorking_hours_start(), shedule.getWorking_hours_start()) &&
                Objects.equals(getWorking_hours_end(), shedule.getWorking_hours_end()) &&
                Objects.equals(getShedule_date(), shedule.getShedule_date());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_shedule(), getId_doctor(), getWorking_hours_start(), getWorking_hours_end(), getShedule_date());
    }


    @Override
    public String toString() {
        return "Shedule{" +
                "id_shedule=" + id_shedule +
                ", id_doctor=" + id_doctor +
                ", working_hours_start=" + working_hours_start +
                ", working_hours_end=" + working_hours_end +
                ", shedule_date=" + shedule_date +
                '}';
    }
}
