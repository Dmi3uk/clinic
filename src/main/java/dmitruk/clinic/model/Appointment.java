package dmitruk.clinic.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "appointment")
public class Appointment {

    @Id
    @Column(name = "id_appointment")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_appointment;

    @Column(name = "id_doctor")
    private int id_doctor;

    @Column(name = "id_patient")
    private int id_patient;

    @Column(name = "appointment_date")
    private LocalDate appointment_date;

    @Column(name = "appointment_time")
    private LocalTime appointment_time;

    @Column(name = "duration_minute")
    private LocalTime duration_minute;

    @Column(name = "id_room")
    private int id_room;

    public int getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(int id_appointment) {
        this.id_appointment = id_appointment;
    }

    public int getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(int id_doctor) {
        this.id_doctor = id_doctor;
    }

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public LocalDate getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(LocalDate appointment_date) {
        this.appointment_date = appointment_date;
    }

    public LocalTime getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(LocalTime appointment_time) {
        this.appointment_time = appointment_time;
    }

    public LocalTime getDuration_minute() {
        return duration_minute;
    }

    public void setDuration_minute(LocalTime duration_minute) {
        this.duration_minute = duration_minute;
    }

    public int getId_room() {
        return id_room;
    }

    public void setId_room(int id_room) {
        this.id_room = id_room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Appointment)) return false;
        Appointment that = (Appointment) o;
        return getId_appointment() == that.getId_appointment() &&
                getId_doctor() == that.getId_doctor() &&
                getId_patient() == that.getId_patient() &&
                getId_room() == that.getId_room() &&
                Objects.equals(getAppointment_date(), that.getAppointment_date()) &&
                Objects.equals(getAppointment_time(), that.getAppointment_time()) &&
                Objects.equals(getDuration_minute(), that.getDuration_minute());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_appointment(), getId_doctor(), getId_patient(), getAppointment_date(), getAppointment_time(), getDuration_minute(), getId_room());
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id_appointment=" + id_appointment +
                ", id_doctor=" + id_doctor +
                ", id_patient=" + id_patient +
                ", appointment_date=" + appointment_date +
                ", appointment_time=" + appointment_time +
                ", duration_minute=" + duration_minute +
                ", id_room=" + id_room +
                '}';
    }
}
