package dmitruk.clinic.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @Column(name = "id_patient")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_patient;

    @Column(name = "patient_name")
    private String patient_name;

    @Column(name = "patient_birthday")
    private LocalDate patient_birthday;

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public LocalDate getPatient_birthday() {
        return patient_birthday;
    }

    public void setPatient_birthday(LocalDate patient_birthday) {
        this.patient_birthday = patient_birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return getId_patient() == patient.getId_patient() &&
                Objects.equals(getPatient_name(), patient.getPatient_name()) &&
                Objects.equals(getPatient_birthday(), patient.getPatient_birthday());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_patient(), getPatient_name(), getPatient_birthday());
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id_patient=" + id_patient +
                ", patient_name='" + patient_name + '\'' +
                ", patient_birthday=" + patient_birthday +
                '}';
    }
}
