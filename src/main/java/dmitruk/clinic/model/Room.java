package dmitruk.clinic.model;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @Column(name = "id_room")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_room;

    @Column(name = "value")
    private int value;

    @Column(name = "id_department")
    private int id_department;

    public int getId_room() {
        return id_room;
    }

    public void setId_room(int id_room) {
        this.id_room = id_room;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId_department() {
        return id_department;
    }

    public void setId_department(int id_department) {
        this.id_department = id_department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return getId_room() == room.getId_room() &&
                getValue() == room.getValue() &&
                getId_department() == room.getId_department();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_room(), getValue(), getId_department());
    }

    @Override
    public String toString() {
        return "Room{" +
                "id_room=" + id_room +
                ", value=" + value +
                ", id_department=" + id_department +
                '}';
    }
}
