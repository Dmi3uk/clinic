package dmitruk.clinic.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "doctor")
public class Doctor {
    @Id
    @Column(name = "id_doctor")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_doctor;

    @Column(name = "doctor_name")
    private String doctor_name;

    @Column(name = "doctor_photo")
    private String doctor_photo;

    @Column(name = "id_specialty")
    private int id_specialty;

    @Column(name = "id_department")
    private int id_department;

    public int getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(int id_doctor) {
        this.id_doctor = id_doctor;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDoctor_photo() {
        return doctor_photo;
    }

    public void setDoctor_photo(String doctor_photo) {
        this.doctor_photo = doctor_photo;
    }

    public int getId_specialty() {
        return id_specialty;
    }

    public void setId_specialty(int id_specialty) {
        this.id_specialty = id_specialty;
    }

    public int getId_department() {
        return id_department;
    }

    public void setId_department(int id_department) {
        this.id_department = id_department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return id_doctor == doctor.id_doctor &&
                id_specialty == doctor.id_specialty &&
                id_department == doctor.id_department &&
                Objects.equals(doctor_name, doctor.doctor_name) &&
                Objects.equals(doctor_photo, doctor.doctor_photo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_doctor, doctor_name, doctor_photo, id_specialty, id_department);
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id_doctor=" + id_doctor +
                ", doctor_name='" + doctor_name + '\'' +
                ", doctor_photo='" + doctor_photo + '\'' +
                ", id_specialty=" + id_specialty +
                ", id_department=" + id_department +
                '}';
    }
}
