package dmitruk.clinic.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "specialty")
public class Specialty {
    @Id
    @Column(name = "id_specialty")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_specialty;

    @Column(name = "value")
    private int value;

    public int getId_specialty() {
        return id_specialty;
    }

    public void setId_specialty(int id_specialty) {
        this.id_specialty = id_specialty;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Specialty)) return false;
        Specialty specialty = (Specialty) o;
        return getId_specialty() == specialty.getId_specialty() &&
                getValue() == specialty.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_specialty(), getValue());
    }

    @Override
    public String toString() {
        return "Specialty{" +
                "id_specialty=" + id_specialty +
                ", value=" + value +
                '}';
    }
}
