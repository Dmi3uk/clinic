package dmitruk.clinic.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "department")
public class Department {

    @Id
    @Column(name = "id_department")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_department;

    @Column(name = "value")
    private int value;

    public int getId_department() {
        return id_department;
    }

    public void setId_department(int id_department) {
        this.id_department = id_department;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return getId_department() == that.getId_department() &&
                getValue() == that.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_department(), getValue());
    }

    @Override
    public String toString() {
        return "Department{" +
                "id_department=" + id_department +
                ", value=" + value +
                '}';
    }
}


