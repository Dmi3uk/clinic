package dmitruk.clinic.dao;

import dmitruk.clinic.model.Doctor;

import java.util.List;

public interface ClinicDAO {
    List<Doctor> allDoctors(int page);
    int doctorsCount();

}
