package dmitruk.clinic.dao;

import dmitruk.clinic.model.Doctor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClinicDaoImpl implements ClinicDAO{
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Doctor> allDoctors(int page) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Doctor").setFirstResult(10 * (page - 1)).setMaxResults(10).list();
    }


    @Override
    public int doctorsCount() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select count(*) from Doctor", Number.class).getSingleResult().intValue();
    }

}
