<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Clinic</title>

</head>
<body>
<table>
    <caption>Clinic</caption>
    <c:if test="${doctorsCount > 0}">
        <tr>
            <th>#</th>
            <th>doctor_name</th>
            <th>doctor_photo</th>
            <th>id_specialty</th>
            <th>id_department</th>
        </tr>
        <c:forEach var="doctor" items="${doctorsList}" varStatus="i">
            <tr>
                <td >${i.index + 1 + (page - 1) * 10}</td>
                <td>${doctor.doctor_name}</td>
                <td><img src="${doctor.doctor_photo}"</td>
                <td>${doctor.id_specialty}</td>
                <td>${doctor.id_department}</td>

        </c:forEach>
    </c:if>
    </tr>


    <c:if test="${patientCount > 0}">
    <tr>
        <th>#</th>
        <th>doctor_name</th>
        <th>doctor_photo</th>
        <th>id_specialty</th>
        <th>id_department</th>
    </tr>
    <c:forEach var="doctor" items="${doctorsList}" varStatus="i">
    <tr>
        <td >${i.index + 1 + (page - 1) * 10}</td>
        <td>${doctor.doctor_name}</td>
        <td><img src="${doctor.doctor_photo}"</td>
        <td>${doctor.id_specialty}</td>
        <td>${doctor.id_department}</td>

        </c:forEach>
        </c:if>
    </tr>

</table>
</body>
</html>


